;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;VARIABLES NEEDED;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Program Stack
(define stack '())

; The function table - a list of lists
(define functions '())

;Temp variable
(define temp "")

;The last boolean used by the client
(define lastBoolean "")

;The last number that was used before an operator was called (For use in loops and conditionals)
(define lastNumberBeforeOperator "")

;Temp variable to hold what function has been called
(define currFunction "")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;MAIN;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;This is the main starting function it displays the prompt and makes the first call to parseList
(define (main)
  (define tempList '())
  (display "UofL>")
  (set! tempList (read-keyboard-as-list))
  (if (not tempList) 
      (begin
        (display "Goodbye")
        (set! stack '())
        )
      (begin
        (parseList (LTL tempList))
        (main)
        )
      )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PARSE FUNCTIONS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;LTL will return a list of strings. parseList will call parse on each of these individual strings
;If a string is "IF" or "LOOP" or "FUNC" it is dealt with here. These keywords require the entire list of strings in order to work.
(define (parseList alist)
  (if (pair? alist)
      (begin
        (cond
          ;Conditionals
          ((equal? (car alist) "IF") 
           (if (eqv? 1 (car stack))
               (begin ;Evaluate IF to Else
                 (parseList (reverse (cdr (reverse (cdr (getBlock alist '() "IF" "ELSE"))))))
                 (parseList (getRestAfterBlock (append (list "ELSE") (getRestAfterBlock alist '() "IF" "ELSE")) '() "ELSE" "THEN"))
                 )
               (begin
                 (parseList (reverse (cdr (reverse (cdr (getBlock (append (list "ELSE") (getRestAfterBlock alist '() "IF" "ELSE")) '() "ELSE" "THEN"))))))
                 (parseList (getRestAfterBlock (append (list "ELSE") (getRestAfterBlock alist '() "IF" "ELSE")) '() "ELSE" "THEN")); Evaluate Else to Then
                 )
          ))
          ;If a loop as been entered
          ((equal? (car alist) "LOOP") (LOOP lastNumberbeforeOperator lastBoolean (reverse (cdr (reverse (cdr (getBlock alist '() "LOOP" "POOL")))))) (parseList (getRestAfterBlock alist '() "LOOP" "POOL")))
  
          ;If a function has been entered 
          ((equal? (car alist) "FUNC") (set! functions (append (list (list (cadr alist) (storeUntil (cddr alist) "CNUF"))) functions)) (set! alist (ignoreUntil alist "CNUF")) (display "Functions:\n") (printFunctions functions) (parseList alist))
          (else (parse (car alist)) (parseList (cdr alist)))
          )
        )
      )
  ) 

;Function to parse individual strings
(define (parse astring)
  (cond 
    ((function? astring functions) (parseList currFunction)) ;Test to see if a keyword is a function call
    
    ((equal? astring "DROP") (set! stack (DROP stack)))
    
    ((equal? astring ".") (display (car stack)) (display #\newline))
    
    ((equal? astring "POP") (POP))
    
    ((equal? astring "SAVE") (set! stack (PUSH temp)))
    
    ((equal? astring "DUP") (set! stack (PUSH (car stack))))
    
    ((equal? astring "SWAP") (if (> (length stack) 1) (set! stack (append (list (cadr stack)) (append (list (car stack)) (cddr stack))))
                       (display "Error: One element or less on stack") ;else
                    )
                   )
    
    ((equal? astring "STACK") (PRINTSTACK stack) (display #\newline))
    
    ((equal? astring "CLEAR") (set! stack '()))
    
    ((number? astring) (set! stack (PUSH astring)))
    ((string->number astring) (set! stack (PUSH (string->number astring))))
    
    ((operator? astring) (if (> (length stack) 1) (begin (set! lastNumberBeforeOperator (car stack)) (set! stack (PUSH (eval (cadr stack) (car stack) astring))  ) (set-cdr! stack (cdddr stack)))
                       (display "Error: Too few operands")))
    ((char=? (string-ref astring 0) #\") (display astring) (display #\newline))
    (else (display "Invalid Entry: Function ") (display astring) (display " does not exist") (display #\newline))
    )
)
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;FUNCTIONS NEEDED BY PARSE;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Pops an item off the stack and saves it in a temporary variable 
(define (POP)
  (set! temp (car stack)) 
  (set! stack (DROP stack))
  )

;Prints the entire stack
(define (PRINTSTACK alist)
  (if (pair? alist) 
      (begin    ;if
        (display (car alist))
        (display " ")
        (if (not (null? (cdr alist)))
            (PRINTSTACK (cdr alist)))
      )
      (display alist) ;else nothing on stack print empty
  )
)
 
;Pushes a number onto the stack
(define (PUSH num)
  (append (list num) stack)
  )

;Swaps the two elements on the top of the stack 
(define (SWAP)
  (define temporary (car (cdr stack))) ;store 2nd element to temporary
  (set-car! (cdr stack) (car stack))  ; set 2nd element to first element
  (append (list temporary) stack)  ; 
  )

;Drops item off the top of the stack, is not saved temporarily 
(define (DROP stack) (cdr stack))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;FUNCTIONS NEEDED BY PARSELIST;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Main loop function. Given a body and the two conditions for a loop it will loop until those conditions leave a 0 on the stack when parsed.
(define (LOOP LCN LCB body)
  (parse LCN)
  (parse LCB)
  (if (= (car stack) 1)
      (begin
        (parseList body)
        (LOOP LCN LCB body)
        )
      )
) 

;This function will print our list of functions in a readable manner
(define (printFunctions alist)
  (if (pair? alist)
      (begin
        (display (caar alist))
        (display #\newline)
        (printFunctions (cdr alist))
      )
  )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;TYPE CHECKING FUNCTIONS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;This will see if a string is a valid function (Defined in our list of functions)
(define (function? name alist)
  (if (pair? alist)
  (if (equal? name (caar alist))
      (begin
        (set! currFunction (cadar alist))
        #t
        )
      (function? name (cdr alist))
      ) #f
        )
  )

;This will return whether or not a string is a operator 
(define (operator? op)
  (cond 
    ((equal? op "+") #t)
    ((equal? op "-") #t)
    ((equal? op "*") #t)
    ((equal? op "/") #t)
    ((equal? op "=") #t)
    ((equal? op ">") #t)
    ((equal? op "<") #t)
    ((equal? op ">=") #t)
    ((equal? op "<=") #t)
    (else #f))
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;<do>UNTIL FUNCTIONS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Given a list it will create a new list of everything in that list up until the stopping condition
(define (storeUntil alist stopCond)
  (if (pair? alist) 
      (if (equal? (car alist) stopCond)
          '();(cdr alist) 
          (begin
            (cons (car alist) (storeUntil (cdr alist) stopCond))
            )
          )
      '() )
  )

;Given a list and a stopping condition this will call parse on each individual string until it reaches the stop condition
(define (readUntil alist stopCond)
  (if (pair? alist) 
      (if (equal? (car alist) stopCond)
          (cdr alist)
          (begin
            (parse (car alist))
            (readUntil (cdr alist) stopCond)
            )
          )
      '() )
  )

;Given a list and a stopping condition this will return the rest of the list after that stopCond
(define (ignoreUntil alist stopCond)
  (if (pair? alist)
      (if (equal? (car alist) stopCond)
          (cdr alist)
          ;else
          (ignoreUntil (cdr alist) stopCond)
          )
      '() ;else
      )
  )

;Given two numbers and a string operator this function will return the result of the evaluation of the two numbers and the operator
;This could be a number (in the case of "+" and etc) or a truthy value. 1 for true, 0 for false. 
(define (eval num1 num2 op)
  (cond 
    ((equal? op "+") (+ num1 num2))
    ((equal? op "-") (- num1 num2))
    ((equal? op "*") (* num1 num2))
    ((equal? op "/") (/ num1 num2))
    ((equal? op "=") (set! lastBoolean "=") (if (= num1 num2) 1 0))
    ((equal? op ">") (set! lastBoolean ">") (if (> num1 num2) 1 0))
    ((equal? op "<") (set! lastBoolean "<") (if (< num1 num2) 1 0))
    ((equal? op ">=") (set! lastBoolean ">=")(if (>= num1 num2) 1 0))
    ((equal? op "<=") (set! lastBoolean "<=") (if (<= num1 num2) 1 0))
  )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;INPUT FUNCTIONS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Takes input from a keyboard as a list of characters
;Given to us by Prof. Zhang
;Edited slightly so that a EOF object returns false
(define (read-keyboard-as-list)    ; this function returns keyboard input as a list
            (let ((char (read-char)))
              (cond 
                ((eof-object? char) #f)
                ((char=? char #\newline) '())
                (else
                 (let loop ((char char))
                   (if (char=? char #\newline)
                       '()
                       (cons char (loop (read-char)))
                       )
                   )         
                 )
                )
              )
  
)


;Transforms a list of characters '(#\a #\p #\p #\l #\e) into a a list of strings '("apple")
(define (LTL alist)
  (if (pair? alist)
      (cond ((char=? (car alist) #\space) (LTL (cdr alist)))
            ((char=? (car alist) #\") (append (storeMessage alist) (LTL (ignoreMessage alist)))) ;If the current input will be a string
            (else (append (list (list->string (LTLHelper alist))) (LTL (Shaving alist))))
       )
       '() ;else
   )
)

;Helper function will return a list of characters up until the first space
(define (LTLHelper alist)
  (if (and (pair? alist) (not (char=? (car alist) #\space)))
      (cons (car alist) (LTLHelper (cdr alist)))
      ;else
      '()
      ))

;Removes everything in a list until it sees a #\space
(define (Shaving alist)
  (if (pair? alist)
      ;If it's a list
      (if (not (char=? (car alist) #\space))
          (Shaving (cdr alist) )
      (cdr alist)
      )
      alist
  )
)

;Used when we need to store a string message that has been input
;Stores an entire string including opening and closing quotation marks
(define (storeMessage alist)
  (list (list->string (append (list #\")(storeMessageHelper (cdr alist)))))
)

;Returns a list of the characters until it sees a quotation mark Eg. (storeMessageHelper '(#\H #\E #\L #\L #\O #\" #\H #\H)) returns (#\H #\E #\L #\L #\O #\")
(define (storeMessageHelper alist)
  (if (char=? (car alist) #\")
      '(#\")
      (append (list (car alist)) (storeMessageHelper (cdr alist)))
      )
  )

;Ignores everything up to an including a quotation mark and returns the rest after that
;Basically, this function will cut off the first quotation mark and the helper will do the rest
(define (ignoreMessage alist)
  (ignoreMessageHelper (cdr alist)))


;Ignores everything up until it reaches a quotation mark 
(define (ignoreMessageHelper alist)
  (if (char=? (car alist) #\")
      (cdr alist)
      (ignoreMessageHelper (cdr alist))
      )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;LOOP/CONDITIONAL HELPER FUNCTIONS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;This function will use a stack to return an entire block given a starting condition and ending condition
;Much like how a stack can be used to grab everything in nested brackets we use a stack here to get nested Loops/Conditionals
;startCond would be a "(" or "LOOP" and the endCond would be ")" or "POOL"
(define (getBlock alist astack startCond endCond)
  (cond 
    ((string=? startCond (car alist)) (set! astack (append (list 1) astack)))
    ((string=? endCond (car alist)) (set! astack (cdr astack)))
    )
  (if (null? astack)
      (cons (car alist) '())
      (cons (car alist) (getBlock (cdr alist) astack startCond endCond))
      )
  )
  
;Similar to the getBlock function this function will return everything -after- that block
(define (getRestAfterBlock alist astack startCond endCond)
  (cond
    ((string=? startCond (car alist)) (set! astack (append (list 1) astack)))
    ((string=? endCond (car alist)) (set! astack (cdr astack)))
  )
  (if (null? astack)
      (cdr alist)
      (getRestAfterBlock (cdr alist) astack startCond endCond))
  )
   
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;TEST LISTS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define test '("LOOP" "DROP" "DUP" "POP" "*" "SAVE" "1" "-" "DUP" "POOL" "DID" "YOU" "MAKE" "IT?"))

(define listy '("LOOP" "DROP" "DUP" "1" "1" "<=" "80" "LOOP" "DROP" "POP" "DUP" "SAVE" "SWAP" "POP" "DUP" "SAVE" "SWAP" "compare" "1" "+" "DUP" "POOL" "DROP" "DROP" "DROP" "1" "+" "DUP" "POOL" "OTHER" "STUFF"))
(define listy2 '("LOOP" "DROP" "POP" "DUP" "SAVE" "SWAP" "POP" "DUP" "SAVE" "SWAP" "compare" "1" "+" "DUP" "POOL" "DROP" "DROP" "DROP" "1" "+" "DUP" "POOL" "OTHER" "STUFF"))
(define listy1 '("HELLO" "MY" "NAME" "IS" "HELLO" "MY" "NAME" "IS" "JON"))
(define listy3 '("POOL"))
(define listy4 '("LOOP" "POOL" "POOL"))
(define listy5 '("POOL" "LOOP" "POOL"))
(define listy6 '("LOOP" "LOOP" "POOL" "LOOP" "POOL" "POOL"))
(define listy7 '("LOOP" "LOOP" "POOL" "POOL" "LOOP" "POOL"))
  